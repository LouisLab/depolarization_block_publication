# Usage

These script enable plotting of the indicated figures. The raw data is found here:

https://doi.org/10.25349/D92K69

In every script, make sure to have the 'root_path' or similar variable point to the correct folder as 
this depends on where exactly on your computer you put the data. 

To save the plots (instead of just showing them in the jupyter ntoebook), please set the variable 
'SAVE_PLOTS = True' in the file 'shared_functions.py' in line 37.

---
## Installation

Install either Miniconda (https://conda.io/miniconda.html) or
Anaconda (https://www.anaconda.com/download/).

Install Git (https://git-scm.com/downloads) (already installed on
many Linux distributions!)

Open Terminal and type

`git clone https://gitlab.com/LouisLab/depolarization_block_publication`

`conda create --name depol_block_publication -y`

Windows:

`activate depol_block_publication`

MacOS:

`source activate depol_block_publication`

Then

`conda install jupyter -y`

`conda install matplotlib -y`

`conda install pandas -y`

`pip install pyabf`

`conda install -c anaconda scipy -y`

`conda install scikit-image -y`

`conda install -c anaconda statsmodels -y`

`conda install -c conda-forge scikit-posthocs -y`

`conda install -c anaconda natsort -y`

---

## Running scripts

Windows:

`activate depol_block_publication`

MacOS:

`source depol_block_publication VS_env`

Then:

`jupyter notebook`

Your browser will open and you can navigate to the script you want to use. 