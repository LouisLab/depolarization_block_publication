"""
For the paper.

Using shared functions makes sure I don't accidentally change settings
between plots/figures when doing the same analysis.
"""

import matplotlib
matplotlib.rcParams['svg.fonttype'] = 'none'
font = {'family' : 'Arial',
    'weight' : 'medium',
    'size'   : 20,
    'style'  : 'normal'}

matplotlib.rc('font', **font)
# The above is necessary to have all text in the saved svg as Arial text.
# Makes modification of figure in illustrator much easier.

import pyabf
import pandas as pd
import numpy as np
from scipy import stats
import statsmodels.api as sm
import matplotlib.pyplot as plt
from pathlib import Path
import json
from mpl_toolkits.axes_grid1.anchored_artists import AnchoredSizeBar
import matplotlib.font_manager as fm
fontprops = fm.FontProperties(size=18)
from statsmodels.stats.multicomp import pairwise_tukeyhsd
import scikit_posthocs
import pickle
from matplotlib.patches import Rectangle
from scipy.stats import bootstrap
import matplotlib.ticker as plticker

SAVE_PLOTS = False # Set to True in order to save plots in the code found in 'code_to_plot_figures'

def collect_data(datapath):
    """
    Collect distance to source data from a given folder.

    Cleans up missing values if tracking failed or at the last 3 frames
    of each experiments by substituting the value with a np.nan.
    """
    no_of_folders = 0
    for folder in datapath.iterdir():
        if folder.is_dir():
            no_of_folders += 1

    max_coordinates = np.zeros((2, no_of_folders))
    all_arenas = None

    counter = 0
    for current_folder in datapath.iterdir():
        if current_folder.is_dir():
            files_of_interest = []
            dst = None
            for current_file in current_folder.iterdir():
                # Since there's the suboptimal naming convention when
                # collecting a video of 'data.csv' to denote info about the
                # stimulus and when then doing analysis it's also called
                # 'data.csv' the loop below first collects all data.csv names
                # and selects the newest one (as analysis must have been done
                # after the data collection)
                if 'data.csv' in current_file.name:
                    files_of_interest.append(current_file)

                if '1296x972_gaussian_center.csv' in current_file.name:
                        arena = pd.read_csv(current_file, sep=',')
                        try:
                            all_arenas.append(arena)
                        except AttributeError:
                            all_arenas = []
                            all_arenas.append(arena)
                    # data_name = i

                if 'distance_to' in current_file.name:
                    dst = pd.read_csv(current_file, sep=',')
            # This is a safety mechanism:
            # If it fails, the dst file is missing in the experimental folder and
            # the user needs to first run the PiVR DST analysis!
            dst.iloc[0] + 1

            if len(files_of_interest) == 1:
                data_name = files_of_interest[0]
            else:
                files_of_interest.sort()
                data_name = files_of_interest[-1]
                # print(data_name)

            data_csv = pd.read_csv(data_name)
            # PiVR currently has some suboptimal behavior where zeros (0) are
            # used to indicate where tracking has failed.
            # To make sure no unexpected data is produced, I'll change them here to nan
            zero_indeces = np.where(data_csv['X-Centroid'] == 0)[0]
            for current_column in data_csv.columns:
                if not current_column == 'Frame':
                    # print(current_column)
                    # Remember: Better to use loc compared to chained indexing!
                    # https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
                    # data_csv[current_column][zero_indeces] = np.nan
                    data_csv.loc[zero_indeces, current_column] = np.nan
            # Also, super important, set dst to zero!!
            dst.loc[zero_indeces] = np.nan

            if counter == 0:
                all_dst = np.zeros((dst.shape[0], no_of_folders))
                all_dst.fill(np.nan)

            with open((Path(current_folder, 'experiment_settings.json')),'r') as file:
                experiment_settings = json.load(file)
                fps = experiment_settings['Framerate']
                pixel_per_mm = experiment_settings['Pixel per mm']
                #max_coordinates[:, counter] = experiment_settings['Source y'], \
                #                              experiment_settings['Source x']

                if 'Source y' in experiment_settings:
                    max_coordinates[:,counter] = experiment_settings['Source y'], \
                                                 experiment_settings['Source x']
                else:
                    # find the coordinates with the max value
                    # only take the first, discard the rest. That's why only
                    # gaussian gradients with should be used here!
                    source_y = np.where(arena == np.nanmax(arena))[0][0]
                    source_x = np.where(arena == np.nanmax(arena))[1][0]
                    max_coordinates[:, counter] = source_y, source_x
                image_res = experiment_settings['Resolution']

            if counter == 0:
                all_pixel_per_mm = np.zeros(no_of_folders)
                all_fps = np.zeros(no_of_folders)
            all_pixel_per_mm[counter] = pixel_per_mm
            all_fps[counter] = fps

            y = np.asarray(data_csv['Y-Centroid'])
            x = np.asarray(data_csv['X-Centroid'])
            if counter == 0:
                centroid_coordinates = np.zeros(
                    (x.shape[0], 2, no_of_folders))

            if x.shape[0] < centroid_coordinates.shape[0]:
                centroid_coordinates[0:x.shape[0], 0, counter] = y
                centroid_coordinates[0:x.shape[0], 1, counter] = x

            elif x.shape[0] > centroid_coordinates.shape[0]:
                temp = centroid_coordinates.copy()

                centroid_coordinates = np.zeros(
                    (x.shape[0], 2, no_of_folders))

                centroid_coordinates[0:temp.shape[0], :, :] = temp

                centroid_coordinates[:, 0, counter] = y
                centroid_coordinates[:, 1, counter] = x

            else:
                centroid_coordinates[:, 0, counter] = y
                centroid_coordinates[:, 1, counter] = x

            if dst.shape[0] < all_dst.shape[0]:
                all_dst[0:dst.shape[0], counter] = dst['0']

            elif dst.shape[0] > all_dst.shape[0]:

                temp_dst = all_dst.copy()
                all_dst = np.zeros((dst.shape[0], no_of_folders))
                all_dst.fill(np.nan)
                all_dst[0:temp_dst.shape[0], :] = temp_dst
                all_dst[:, counter] = dst['0']
            else:
                all_dst[:, counter] = dst['0']

            counter += 1

    # Set all zeros to np.nan as zeros mean that animals was not tracked during that frame.
    centroid_coordinates[centroid_coordinates == 0] = np.nan

    if all_arenas is None:

        return (centroid_coordinates, max_coordinates, all_dst, image_res,
                all_pixel_per_mm, all_fps)

    else:

        return (centroid_coordinates, max_coordinates, all_dst, image_res,
                all_pixel_per_mm, all_fps, all_arenas)

def plot_trajectories(centroids, center, savename, plotcolor, res, px_per_mm, fps,
         take_time_end = None, black_background=False, all_arena=None,
          savepath = None, single_trajectory=None, odor_color='gold'):
    """
    Plots centered trajectories.
    """
    if res == '1024x768':
        image_res_x = 1024
        image_res_y = 768
    elif res == '1296x972':
        image_res_x = 1296
        image_res_y = 972

    # To show the same in the plot as in the stats I need to adjust the duration of what I show
    # to fit with the stats.plot_trajectories
    if take_time_end is None:
        # take all
        # Here we create an array with the length of the number of
        # experiments and filled with the maximum possible length
        take_time_end_array = np.array(([centroids.shape[0]]*centroids.shape[2]))
    else:
        # Here we create an array with the length of the number of
        # experiments and filled with the desired length
        take_time_end_array = np.array(([take_time_end]*centroids.shape[2]))
        # take_time_start *= framerate
        take_time_end_array *= fps

    # Plotting starts
    fig, ax = plt.subplots()

    if single_trajectory is not None:
        # center the maximum value and the corresponding x/y values
        y_shift = (image_res_y / 2 - center[0, single_trajectory])
        x_shift = (image_res_x / 2 - center[1, single_trajectory])
        shifted_y = centroids[:, 0, single_trajectory] + y_shift
        shifted_x = centroids[:, 1, single_trajectory] + x_shift

        if black_background:
            ax.scatter(y=shifted_y[0:take_time_end_array[single_trajectory]],
                       x=shifted_x[0:take_time_end_array[single_trajectory]],
                       s=0.0001, c='w'
                       )
        else:
            ax.plot(shifted_x[0:take_time_end_array[single_trajectory]],
                    shifted_y[0:take_time_end_array[single_trajectory]],
                    c=plotcolor, alpha=1, lw=0.5)

        # also plot outline of the experimental dish:
        dish_width = 127  # in mm, from manufacturer
        dish_height = 85  # in mm, from manufacturer
        # bring everything to pixel dimension
        dish_width *= px_per_mm[single_trajectory]
        dish_height *= px_per_mm[single_trajectory]

        dish_x_start = center[1, single_trajectory] - (dish_width / 2)
        dish_y_start = center[0, single_trajectory] - (dish_height / 2)

        rect = Rectangle((dish_x_start, dish_y_start), dish_width,
                         dish_height,
                         fill=None)

        ax.add_patch(rect)

    else:
        # center the maximum value and the corresponding x/y values
        for i in range(center.shape[1]):
            y_shift = (image_res_y / 2 - center[0, i])
            x_shift = (image_res_x / 2 - center[1, i])
            shifted_y = centroids[:, 0, i] + y_shift
            shifted_x = centroids[:, 1, i] + x_shift

            if black_background:
                ax.scatter(y=shifted_y[0:take_time_end_array[i]],
                           x=shifted_x[0:take_time_end_array[i]],
                           s=0.0001, c='w'
                           )
            else:
                ax.plot(shifted_x[0:take_time_end_array[i]],
                        shifted_y[0:take_time_end_array[i]],
                        c=plotcolor, alpha=0.25, lw=0.5)

    # In case a virtual arena (VR) was used, plot it.
    if all_arena is not None:
        ax.imshow(np.nanmean(all_arena,axis=0), cmap='Reds', alpha=0.3)

    # Add indication of center
    diameter_of_reinforcement_ring = 6.35 # 1/4 inch according to manufacturer
    radius_of_reinforcement_ring = diameter_of_reinforcement_ring/2
    pixel_per_mm = np.mean(px_per_mm) # Pixel per mm is roughly the same

    circle1 = plt.Circle((image_res_x/2, image_res_y/2),
                         radius_of_reinforcement_ring * pixel_per_mm,
                         color=odor_color, alpha = 1, zorder=0)
    ax.add_patch(circle1)
    # Add scalebar of 10 mm
    scalebar = AnchoredSizeBar(ax.transData,
                               px_per_mm[0] * 10, '10 mm', 'lower center',
                               pad=0.1,
                               color='k',
                               frameon=False,
                               size_vertical=0.01,
                               fontproperties=fontprops)

    ax.add_artist(scalebar)
    # make the plot pretty
    ax.set_ylim(0, image_res_y)
    ax.set_xlim(0, image_res_x)
    ax.axis('off')
    # This fixes the numpy/matplotlib behavior that inverts the y axis
    # when images are plotted
    ax.invert_yaxis()
    if SAVE_PLOTS:
        fig.savefig(Path(savepath, savename))


def doublecheck_fps(fps):
    """
    Initially I used 15fps but then moved to recording behavioral
    data at 20 fps. I believe that each dataset is internally consistent
    but this function should help catch any potential mixup!
    """
    if fps.mean() == fps[0]:
        print('All files have ' + repr(fps.mean()) + ' fps')
    else:
        print('WARNING: Check fps!')


def plot_DST_boxplot(data, label, colors, framerate):
    """
    Used to plot boxplots of the distance to source measurement. This is
    used for quick overview, not for actual plots in the paper.

    It does return the dataframe containing the distance to source that
    is used to perform statistics. The median of the distance to source
    of the first 3 minutes is calculated per experiment and returned.
    """

    fig = plt.figure()
    ax = fig.add_subplot()

    box_counter = 0
    df_for_stats = pd.DataFrame()
    for i in range(len(data)):

        take_time_start = 0  # seconds
        take_time_end = 180  # always take first 3 minutes of an
        # experiment

        take_time_start *= framerate[i]
        take_time_end *= framerate[i]

        data_to_use = np.nanmedian(data[i][take_time_start:take_time_end, :], axis=0)
        # print(label[i])
        ax.boxplot(data_to_use, positions=[box_counter],
                   notch=True)
        x_positions = np.random.normal(box_counter, 0.02,
                                       size=data[i].shape[1])
        ax.scatter(x=x_positions, y=data_to_use,
                   alpha=0.5, color=colors[i], label=label[i], zorder=0)
        box_counter += 0.25

        dict_for_stats = {
            'Group': [label[i]] * x_positions.shape[0],
            'median_DTS': data_to_use
        }

        df_to_add = pd.DataFrame(dict_for_stats)
        try:
            df_for_stats = pd.concat([df_to_add, df_for_stats],
                                     ignore_index=True)
            df_for_stats.reset_index()
        except Exception as e:
            print(e)
            df_for_stats = df_to_add

    ax.set_xlim(-0.25, box_counter)
    ax.set_ylabel('Distance to Source [mm]')
    ax.tick_params(
        axis='x',  # changes apply to the x-axisplot_boxplots
        which='both',  # both major and minor ticks are affected
        bottom=False,  # ticks along the bottom edge are off
        top=False,  # ticks along the top edge are off
        labelbottom=False)  # labels along the bottom edge are off
    fig.legend()
    ax.set_ylim(0, 100)

    return (df_for_stats)



def estimate_PDF(data_1, framerate_data1, data1_label,
                 data_2, framerate_data2, data2_label,
                 data_3 = None, framerate_data3 = None, data3_label = None,
                 data_4 = None, framerate_data4 = None, data4_label = None,
                 savename = None,
                 color1 = 'green',
                 color2 = 'magenta',
                 color3 = 'k',
                 color4 = 'orange',
                 ylim=(-0.01, 0.2),
                 hist_range=(0,60),
                 lw=0.5,
                 second_linestyle = 'dotted',
                 third_linestype = 'dashed',
                 fourth_linestype = 'dashdot'):
    """
    Histograms are a bit difficult to display the distance to the
    source for the different experiments. We decided to show the
    distribution instead with a probability density function.

    The function requires at least 2 datasets and can handle up to 4.
    """

    start_time = 0
    end_time = 180 # only the first 3 minutes of each dataset are used

    data_1_flat = []
    for counter, fps in enumerate(framerate_data1):
        data_1_flat.extend(data_1[int(start_time * fps):
                                  int(end_time * fps),
                           counter].tolist())
    data_1_flat = np.array(data_1_flat)

    data_2_flat = []
    for counter, fps in enumerate(framerate_data2):
        data_2_flat.extend(data_2[int(start_time * fps):
                                  int(end_time * fps),
                           counter].tolist())
    data_2_flat = np.array(data_2_flat)

    if data_3 is not None:
        data_3_flat = []
        for counter, fps in enumerate(framerate_data3):
            data_3_flat.extend(data_3[int(start_time * fps):
                                      int(end_time * fps),
                               counter].tolist())
        data_3_flat = np.array(data_3_flat)

    if data_4 is not None:
        data_4_flat = []
        for counter, fps in enumerate(framerate_data4):
            data_4_flat.extend(data_4[int(start_time * fps):
                                      int(end_time * fps),
                               counter].tolist())
        data_4_flat = np.array(data_4_flat)

    data_1_flat = data_1_flat[~np.isnan(data_1_flat)]
    data_2_flat = data_2_flat[~np.isnan(data_2_flat)]
    if data_3 is not None:
        data_3_flat = data_3_flat[~np.isnan(data_3_flat)]
    if data_4 is not None:
        data_4_flat = data_4_flat[~np.isnan(data_4_flat)]

    density1 = stats.gaussian_kde(data_1_flat)
    density2 = stats.gaussian_kde(data_2_flat)
    if data_3 is not None:
        density3 = stats.gaussian_kde(data_3_flat)
    if data_4 is not None:
        density4 = stats.gaussian_kde(data_4_flat)

    fig = plt.figure(figsize=(5, 2))
    ax = fig.add_subplot(111)
    # n,x,_ = ax.hist(data1, density=True, bins=100,
    #        color='green', alpha=.5
    #        )
    n, x = np.histogram(data_1_flat, density=True, bins=100, range=hist_range)
    ax.plot(x, density1(x), color=color1, label=data1_label,lw=lw)

    # n,x,_ = ax.hist(data2, density=True, bins=100,
    #        color='m', alpha=.5
    #        )
    n, x = np.histogram(data_2_flat, density=True, bins=100, range=hist_range)
    ax.plot(x, density2(x), color=color2, label=data2_label,
            ls=second_linestyle, lw=lw)

    if data_3 is not None:
        n, x = np.histogram(data_3_flat, density=True, bins=100, range=hist_range)
        ax.plot(x, density3(x), color=color3, label=data3_label,
                ls=third_linestype, lw=lw)
        ax.set_xlabel('Distance [mm]')
    collect_and_plot_ephys_traces
    if data_4 is not None:
        n, x = np.histogram(data_4_flat, density=True, bins=100, range=hist_range)
        ax.plot(x, density4(x), color=color4, label=data4_label,
                ls=fourth_linestype, lw=lw)
        ax.set_xlabel('Distance [mm]')

    ax.set_xlim(-5, 70)
    loc = plticker.MultipleLocator(base=30)  # this locator puts ticks at regular intervals
    ax.xaxis.set_major_locator(loc)
    ax.set_ylim(ylim)

    fig.legend()
    if SAVE_PLOTS:
        fig.savefig(savename)


def collect_and_plot_ephys_traces(path, savename, save_path,
                                  ylim=None,
                                  rasterplot_linethickness = 0.1,
                                  xlim=None,
                                  lineplot_thickness=0.1,
                                  detailed_x1=None, detailed_x1_color=None,
                                  detailed_x2=None, detailed_x2_color=None,
                                  detailed_y_pos = 1,
                                  odor_color='gold',
                                  odor_height = 10,
                                  ):
    """
    This function is used to plot the raw electrophysiology trace.
    """

    for current_file in path.iterdir():

        if '.abf' in current_file.name:
            abf = pyabf.ABF(str(current_file))

        if 'pressure.csv' in current_file.name:
            odor_stim = pd.read_csv(current_file)

            if '_Ch1' in current_file.name:
                pipette_channel = 'Ch1[mbar]_Real'
                odor_stim_index = 0
            elif '_Ch2' in current_file.name:
                pipette_channel = 'Ch2[mbar]_Real'
                odor_stim_index = 1
        if 'final_spikes.pkl' in current_file.name:
            with open(current_file, 'rb') as handle:
                final_spikes = pickle.load(handle)

    # sometimes data is messed up at edges
    recording_data = abf.data[0, :]
    if abf.data.shape[0] == 2:
        stim_data = abf.data[1, :]
    else:
        stim_data = abf.data[2, :]
    time = abf.sweepX
    # Get sampling frequency
    sampling_freq = 1 / time[1]

    # put in same timeframes as the recording data for easy plotting
    expanded_odor_stim = np.zeros((recording_data.shape[0], 4))
    try:
        ob1_tic_toc = abf.data[3, :]

        # find the rising edges to find indeces where the odor stimulus is updated
        trigger_val = 3  # it's about 3.5V
        rising_edges = np.flatnonzero(
            (ob1_tic_toc[:-1] < trigger_val) & (
                    ob1_tic_toc[1:] > trigger_val)) + 1

        for counter, i_tic in enumerate(rising_edges):
            try:
                expanded_odor_stim[i_tic:rising_edges[counter + 1], 0] = \
                    odor_stim['Ch1[mbar]_Real'][counter]
                expanded_odor_stim[i_tic:rising_edges[counter + 1], 1] = \
                    odor_stim['Ch2[mbar]_Real'][counter]
                expanded_odor_stim[i_tic:rising_edges[counter + 1], 2] = \
                    odor_stim['Ch3[mbar]_Real'][counter]
                expanded_odor_stim[i_tic:rising_edges[counter + 1], 3] = \
                    odor_stim['Ch4[mbar]_Real'][counter]
            except IndexError:
                expanded_odor_stim[i_tic, 0] = \
                    odor_stim['Ch1[mbar]_Real'][counter]
                expanded_odor_stim[i_tic, 1] = \
                    odor_stim['Ch2[mbar]_Real'][counter]
                expanded_odor_stim[i_tic, 2] = \
                    odor_stim['Ch3[mbar]_Real'][counter]
                expanded_odor_stim[i_tic, 3] = \
                    odor_stim['Ch4[mbar]_Real'][counter]

    except IndexError:
        # This happens if no odor stimulation
        pass
    odor_start = \
        np.where(expanded_odor_stim[:, odor_stim_index] > 55)[0][0]
    odor_stop = \
        np.where(expanded_odor_stim[:, odor_stim_index] > 55)[0][-1]

    fig = plt.figure(figsize=(10, 5))
    ax = fig.add_subplot(111)

    # Since the trace contains 10,000 points per second the image needs
    # to be rasterized, unfortunately.
    ax.set_rasterization_zorder(2)
    ax.plot(time, recording_data, lw=lineplot_thickness, c='k', zorder=1)

    # Grab current ylim
    if ylim is not None:
        current_ylim = ylim
    else:
        current_ylim = ax.get_ylim()

    ax.set_xlim((odor_start / sampling_freq) - 2,
                (odor_stop / sampling_freq) + 2)

    # Dynamically deal with rasterplot linelength
    linelength = (current_ylim[-1] - current_ylim[0]) * 0.05
    ax.eventplot(
        np.array(final_spikes['target_spikes_1'] / sampling_freq),
        linewidth=rasterplot_linethickness,
        lineoffsets=current_ylim[-1] - (current_ylim[-1]*0.05), color='k',
        linelengths=linelength)

    # Dynamically adjust stimulus indicator and linewidth as not all
    # recordings have the identical ylim
    odor_stim_y_pos = current_ylim[0]
    odor_stim_y_height = (current_ylim[-1] - current_ylim[0]) * 0.05
    odor_lw = (current_ylim[-1] - current_ylim[0]) * odor_height

    ax.hlines(y=odor_stim_y_pos,
              xmin=odor_start/sampling_freq,
              xmax=odor_stop/sampling_freq,
              colors=odor_color, #linestyles='-',
              lw=odor_lw, zorder=10)#, label='Single Short Line')

    if detailed_x1 is not None:
        rectangle_initial_x = detailed_x1[0]
        rectangle_final_x = detailed_x1[-1] - detailed_x1[0]

        ax.add_patch(
            Rectangle((rectangle_initial_x, -detailed_y_pos),
                      rectangle_final_x, detailed_y_pos*2,
                      facecolor=detailed_x1_color, zorder=2,
                      alpha = 0.5))

    if detailed_x2 is not None:
        rectangle_initial_x = detailed_x2[0]
        rectangle_final_x = detailed_x2[-1] - detailed_x2[0]

        ax.add_patch(
            Rectangle((rectangle_initial_x, -detailed_y_pos),
                      rectangle_final_x, detailed_y_pos*2,
                      facecolor=detailed_x2_color, zorder=2,
                      alpha = 0.5))

    # re-set the ylim based on the ylim before manually changing it.
    ax.set_ylim(current_ylim)
    # The below sets the xlim to where Ob1 is active.
    ax.set_xlim((odor_start / sampling_freq) - 2,
                (odor_stop / sampling_freq) + 2)
    if xlim is None:
        time_bar = 5
        time_bar_string = '5 s'
    else:
        total_time = xlim[1] - xlim[0]
        time_bar = total_time / 10 # 10% of time used for timebar
        time_bar_string = repr((round(time_bar * 1000))) + ' ms'
        # the 1000 are used to bring scale from seconds to milliseconds
        # for improved readability

    scalebar = AnchoredSizeBar(ax.transData,
                               time_bar, time_bar_string, 'lower right',
                               pad=0.1,
                               color='k',
                               frameon=False,
                               size_vertical=0.01,
                               fontproperties=fontprops)


    ax.add_artist(scalebar)

    scalebar_volt = AnchoredSizeBar(ax.transData,
                                    0.01, '0.1 mV', 'lower left',
                                    pad=0.1,
                                    color='k',
                                    frameon=False,
                                    size_vertical=0.1,
                                    fontproperties=fontprops)
    ax.add_artist(scalebar_volt)
    if ylim is not None:
        ax.set_ylim(ylim)
    if xlim is not None:
        ax.set_xlim(xlim)
    ax.axis('off')

    if SAVE_PLOTS:
        fig.savefig(Path(save_path, savename), rasterized=True, dpi=300)


def plot_DST_boxplot_grouped(data_to_plot, labels_to_plot, color, savepath, savename,
                             ylim=None, grouping='ctr vs exp',
                             boxplot_width=(0.5)):
    """
    This function is used to plot paired boxplots e.g. in Figure 1F.
    """

    fig = plt.figure(figsize = (len(data_to_plot)*1.5,5))
    ax = fig.add_subplot(111)

    for i, current_data in enumerate(data_to_plot):
        current_x_base = i
        if i != 0:
            current_x_base *= 2 # Since the width of the boxplot is
            # quite large I need more space in x. Done here
        for counter, current_label in enumerate(labels_to_plot[i]):
            #print(counter)
            print(current_label)
            if grouping == 'ctr vs exp':
                if counter == 0:
                    x_position = current_x_base - 0.3
                else:
                    x_position = current_x_base + 0.3

            elif grouping == 'concentrations':
                if counter % 2 == 0:
                    x_position = current_x_base - 0.3
                else:
                    x_position = current_x_base + 0.3

            print(x_position) # keep for debugging

            current_data_to_plot = current_data[current_data['Group'] == current_label]['median_DTS']

            ax.boxplot(current_data_to_plot,
                       positions=[x_position], notch=True, showfliers=False,
                       widths=boxplot_width)

            x_scatter = np.random.normal(x_position, 0.02, size=current_data_to_plot.shape[0])
            if grouping == 'ctr vs exp':
                ax.scatter(x_scatter, current_data_to_plot, c=color[counter], alpha=0.5)
            elif grouping == 'concentrations':
                ax.scatter(x_scatter, current_data_to_plot, c=color[i], alpha=0.5)

    ax.set_xlim(-0.75, x_position + 0.5)
    ax.set_ylabel('Distance to odor [mm]', size=20)

    ax.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False) # labels along the bottom edge are off
    ax.yaxis.set_tick_params(labelsize=15)
    fig.legend(fontsize=15)
    if ylim is not None:
        ax.set_ylim(ylim)

    ax.invert_yaxis()
    if SAVE_PLOTS:
        fig.savefig(Path(savepath, savename))


def plot_boxplots(data_to_plot, labels_to_plot, color, savepath,
                  savename,
                  ylim=None, boxplot_width=(0.5)):
    """
    This is used for overview plotting (not for figures).
    """
    fig = plt.figure(figsize=(len(labels_to_plot[0]) * 1.5, 5))
    ax = fig.add_subplot(111)

    current_data = data_to_plot[0]

    for counter, current_label in enumerate(labels_to_plot[0]):
        # print(counter)
        print(current_label)

        x_position = counter

        x_position /= 1.5
        print(x_position)

        current_data_to_plot = \
            current_data[current_data['Group'] == current_label]['median_DTS']

        ax.boxplot(current_data_to_plot,
                   positions=[x_position], notch=True, showfliers=False,
                   widths=boxplot_width)

        x_scatter = np.random.normal(x_position, 0.02,
                                     size=current_data_to_plot.shape[0])

        ax.scatter(x_scatter, current_data_to_plot, c=color[counter],
                   alpha=0.5)

    ax.set_xlim(-0.75, x_position + 0.5)
    ax.set_ylabel('Distance to odor [mm]', size=20)

    ax.tick_params(
        axis='x',  # changes apply to the x-axis
        which='both',  # both major and minor ticks are affected
        bottom=False,  # ticks along the bottom edge are off
        top=False,  # ticks along the top edge are off
        labelbottom=False)  # labels along the bottom edge are off
    ax.yaxis.set_tick_params(labelsize=15)
    fig.legend(fontsize=15)
    if ylim is not None:
        ax.set_ylim(ylim)

    ax.invert_yaxis()
    if SAVE_PLOTS:
        fig.savefig(Path(savepath, savename))


def collect_median_for_dose_response(root_path,
                                     conc_to_show,
                                     genotype_to_show):


    """
    For Figure 6 collect the median of the mean distance to source
    over the first 3 minutes.

    Also prepares some untested confidence interval calculations which
    were not used in the manuscript.
    """
    time_to_use_start = 0 # seconds
    time_to_use_end = 180 # seconds

    all_median_dst = np.zeros((len(conc_to_show), len(genotype_to_show)))
    all_median_dst.fill(np.nan)

    all_STD_dst = np.zeros((len(conc_to_show), len(genotype_to_show)))
    all_STD_dst.fill(np.nan)

    all_SEM_dst = np.zeros((len(conc_to_show), len(genotype_to_show)))
    all_SEM_dst.fill(np.nan)

    all_CI_high_dst = np.zeros((len(conc_to_show), len(genotype_to_show)))
    all_CI_high_dst.fill(np.nan)

    all_CI_low_dst = np.zeros((len(conc_to_show), len(genotype_to_show)))
    all_CI_low_dst.fill(np.nan)

    conc_in_numbers = []  # for plotting

    # for each concentration defined in list above
    for conc_counter, current_conc in enumerate(conc_to_show):
        if 'mM' in current_conc:
            conc_in_numbers.append(float(current_conc.split('mM')[0].split(' ')[-1]))
        elif 'uM' in current_conc:
            conc_in_numbers.append(float(current_conc.split('uM')[0].split(' ')[-1])/1000)
            #conc_in_numbers[-1] /= 1000
        else:
            print('MISSING CONC IN NUMBERS FOR ' + repr(current_conc))
        current_conc_path = Path(root_path, current_conc)

        # In each concentration, find the genotype(s) we want to show
        for genotype_counter, current_genotype in enumerate(genotype_to_show):

            current_genotype_path = Path(current_conc_path, current_genotype)

            # Not every genotype has data for each experiment!
            try:

                mean_dst = []

                # for each experiment
                for current_experiment in current_genotype_path.iterdir():

                    if current_experiment.is_dir():

                        # find distance to source file
                        for current_file in current_experiment.iterdir():
                            if 'distance_to_source.csv' in current_file.name:

                                dst = pd.read_csv(current_file)

                            if 'experiment_settings.json' in current_file.name:
                                with open(current_file, 'r') as file:
                                    experiment_settings = json.load(file)
                                    fps = experiment_settings['Framerate']

                        current_time_to_use_start = time_to_use_start * fps
                        current_time_to_use_end = time_to_use_end * fps
                        # Get mean of the distance to source for the defined time
                        mean_dst.append(dst.iloc[current_time_to_use_start:current_time_to_use_end, 1].mean())

                all_median_dst[conc_counter, genotype_counter] = np.median(mean_dst)
                all_STD_dst[conc_counter, genotype_counter] = np.std(mean_dst)
                all_SEM_dst[conc_counter, genotype_counter] = all_STD_dst[conc_counter, genotype_counter]/len(mean_dst)

                data = (mean_dst,)  # samples must be in a sequence
                CI = bootstrap(data, statistic=np.std)
                all_CI_high_dst[conc_counter, genotype_counter] = np.median(mean_dst) + CI.confidence_interval[1]
                all_CI_low_dst[conc_counter, genotype_counter] = np.median(mean_dst) - CI.confidence_interval[0]

            except FileNotFoundError:
                print('Folder does not exist: ' + str(current_genotype_path))
    return(all_median_dst, conc_in_numbers, all_STD_dst, all_SEM_dst,
           all_CI_high_dst, all_CI_low_dst)


def mean_spike_rate(path, conc_to_use,
                    time_start=None, time_end=None,
                    data_aquisition_rate=10000):
    """
    Extracts mean spike rate from folders in 'conc_to_use' variable.
    time_start and time_end are indicating (in seconds from time
    of OB1 being active) time where mean is calculated of.
    """
    time_start = int(round(time_start * data_aquisition_rate))
    time_end = int(round(time_end * data_aquisition_rate))

    print(time_start)

    mean_values = []
    odor_conc = []
    std_values = []
    sem_values = []
    for i_conc in conc_to_use:

        print(i_conc)
        current_path = Path(path, i_conc)
        for i_file in current_path.iterdir():

            if '.csv' in i_file.name:
                print(i_file)
                current_PSTH = pd.read_csv(i_file)
        # print(current_PSTH.shape)

        # Take mean in time (PSTH is already the mean across trials)
        mean_values.append(
            current_PSTH['PSTH [Hz]'][time_start:time_end].mean())
        # and take mean of the std for plotting.
        std_values.append(
            current_PSTH['PSTH STD'][time_start:time_end].mean())
        # and also take SEM
        sem_values.append(
            current_PSTH['PSTH SEM'][time_start:time_end].mean())
        odor_conc.append(i_conc)

    return (mean_values, odor_conc, std_values, sem_values)

def plot_PSTH_Fig1(data, odor_conc, savename, ylim, aq_rate=10000,
                   xlim=None, save_path=None,
                   sensory_experience=False, max_OB1_pressure=200,
                   odor_color='gold', lw_odor = 1,lw_PSTH = 0.1,
                   rasterplot_lw = 0.05):
    """
    Used to plot the PSTH of the Or42b/EtB recordings shown in SI Fig 1
    """

    if save_path is None:
        print('please provide savepath')

    try:
        odor = data['pressure [mbar]'] / max_OB1_pressure * odor_conc
    except KeyError:
        # This is only relevant for the 1 mM EtB case
        odor = data['pressure Ch2 [mbar]'] / max_OB1_pressure * odor_conc


    fig = plt.figure()
    ax1 = fig.add_subplot(311)
    ax1.plot(data['Time [s]'], odor, c=odor_color, lw=lw_odor)

    if sensory_experience:
        integration_time = 20000  # 2 seconds
        odor_stim = data['pressure [mbar]'] / 200 * odor_conc # 200 mbar
        # is maximal pressure

        # for performance reasons we can't apply the 1/c*dc/dt
        # calculation to every point.
        # Instead, take the frequency of odor stimulus (50 Hz) and
        # only consider points every 20 ms
        take_every = int(round((1 / 50) / data['Time [s]'][1]))

        sensory_exp = np.zeros((data.shape[0]))
        sensory_exp.fill(np.nan)

        for i_inner in np.arange(integration_time,
                                 sensory_exp.shape[0] - 1, take_every):
            # print(i_inner)
            sensory_exp[i_inner] = (1 / odor_stim.iloc[i_inner]) * \
                                   ((odor_stim.iloc[i_inner] -
                                     odor_stim.iloc[
                                         i_inner - integration_time]) / integration_time)

        # define correct time!
        time_dC = np.linspace(0, [data['Time [s]'][-1]],
                              sensory_exp[~np.isnan(sensory_exp)].shape[
                                  0])

        ax1_right = ax1.twinx()
        ax1_right.plot(time_dC,
                       sensory_exp[~np.isnan(sensory_exp)],
                       c='r')
        ax1_right.set_ylabel(
            '1/c*dc/dt, dt=' + repr(integration_time / aq_rate))

    ax1.set_ylabel('EtB [uM]')
    # ax1_right.set_ylabel('uW/mm2')

    ax2 = fig.add_subplot(312, sharex=ax1)
    ax2.set_rasterization_zorder(1)
    position = 0
    for counter, trial in enumerate(data.columns):
        if 'spikes_' in trial:
            ax2.eventplot(np.where(data[trial] == 1)[0] / aq_rate,
                          linewidth=rasterplot_lw, lineoffsets=position,
                          color='k', zorder=0)
            position += 1

    ax3 = fig.add_subplot(313, sharex=ax1)

    ax3.plot(data['Time [s]'], data['PSTH [Hz]'], c='k', lw=lw_PSTH)
    ax3.plot(data['Time [s]'], data['PSTH [Hz]'] - data['PSTH SEM'],
             c='k', alpha=0.5, lw=lw_PSTH)
    ax3.plot(data['Time [s]'], data['PSTH [Hz]'] + data['PSTH SEM'],
             c='k', alpha=0.5, lw=lw_PSTH)
    ax3.set_ylim(ylim)

    ax3.set_ylabel('Firing rate [Hz]')

    if xlim is not None:
        ax1.set_xlim(xlim)

    ax3.set_xlabel('Time[s]')
    if SAVE_PLOTS:
        fig.savefig(Path(save_path, savename), rasterized=True, dpi=300)


def export_statistics_multiple(data, savepath, savename, parametric=False,
                               data_label='median_DTS',
                               group_label='Group'):
    """
    Calculate and prepare all the statistical calculations presented
    in Data S1.
    """
    labels = data.drop_duplicates(subset=[group_label])[group_label]  # extract unique group names
    label_list = []
    [label_list.append(i) for i in labels]  # put them into a list
    print(label_list)  # a confirmation that the group names are entered correctly!

    std = []
    n = []
    mean = []
    median = []
    sem = []
    ci = []
    lilliefors_test_stat = []
    lilliefors_p = []
    flat_data = []

    for current_label in label_list:
        current_data = data[data_label][data[group_label] == current_label]

        mean.append(np.nanmean(current_data))
        median.append(np.nanmedian(current_data))
        std.append(np.nanstd(current_data))

        n.append(current_data.shape[0])

        sem.append(std[-1] / n[-1])

        ci.append(confidence_interval(current_data))

        lilliefors_test_stat.append(
            sm.stats.lilliefors(current_data)[0])
        lilliefors_p.append(sm.stats.lilliefors(current_data)[-1])

        flat_data.append(current_data)

    # Put data into dataframe for easy export
    df_for_export = pd.DataFrame()

    # df_for_export['Group'] = label_list # Needs to be adapted with dilutions!
    df_for_export[group_label] = label_list
    df_for_export['n'] = n
    df_for_export['mean'] = mean
    df_for_export['median'] = median
    df_for_export['STD'] = std
    df_for_export['SEM'] = sem
    df_for_export['95% CI on the mean'] = ci

    df_for_export['statistical test'] = pd.Series(dtype='string')
    if parametric:
        df_for_export.loc[0, 'statistical test'] = 'ANOVA'
        statistics = stats.f_oneway(*flat_data)
        df_for_export['F statistic'] = pd.Series(dtype='float')
        df_for_export.loc[0, 'F statistic'] = statistics[0]

        df_for_export['p value'] = pd.Series(dtype='float')
        df_for_export.loc[0, 'p value'] = statistics[1]

        # For effect size, calculate eta-square:
        # Tomczak, Maciej, and Ewa Tomczak. "The need to report effect
        # size estimates revisited. An overview of some recommended
        # measures of effect size." Trends in sport sciences 1.21 (2014): 19-25.
        # Formula is: SS_ef / SS_t = eta^2
        # Unfortunately I don't have access to these values from stats.f_oneway...
        # Hence, it needs to be done by hand:
        # Copy/Paste from here: https://www.marsja.se/four-ways-to-conduct-one-way-anovas-using-python/
        k = len(data['Group'].unique())
        N = data.shape[0]
        n = data.groupby('Group').size()[0]

        DFbetween = k - 1 # not used
        DFwithin = N - k # not used

        SSbetween = (sum(data.groupby(group_label).sum()[data_label] ** 2) / n) - (data[data_label].sum() ** 2) / N
        sum_y_squared = sum([value ** 2 for value in data[data_label].values])
        SSwithin = sum_y_squared - sum(data.groupby(group_label).sum()[data_label] ** 2) / n # not used
        SStotal = sum_y_squared - (data[data_label].sum() ** 2) / N
        # F = MSbetween / MSwithin # This can be used as a control to be compared
        # to result of scipy.stats.f_oneway()!
        eta_square = SSbetween / SStotal

    else:
        df_for_export.loc[0, 'statistical test'] = 'Kruskal-Wallis'
        statistics = stats.kruskal(*flat_data)

        # df_for_export['H statistic'] = [stats.kruskal(*flat_data)[0], np.nan,np.nan] # This is less flexible!
        df_for_export['H statistic'] = pd.Series(dtype='float')
        df_for_export.loc[0, 'H statistic'] = statistics[0]

        df_for_export['p value'] = pd.Series(dtype='float')
        df_for_export.loc[0, 'p value'] = statistics[1]

        # For effect size, calculate eta-square:
        # Tomczak, Maciej, and Ewa Tomczak. "The need to report effect
        # size estimates revisited. An overview of some recommended
        # measures of effect size." Trends in sport sciences 1.21 (2014): 19-25.
        # eta^2 = (H - k + 1)/(n-k) with H = H statistic, k = number
        # of groups to be compared and n = number of samples
        eta_square = (statistics[0] - len(label_list) + 1) / (data.shape[0] - len(label_list))

        if stats.kruskal(*flat_data)[1] < 0.05:
            conover_export = scikit_posthocs.posthoc_conover(
                data,
                val_col=data_label,
                group_col=group_label,
                p_adjust='holm')

    if statistics[1] < 0.05:
        df_for_export.loc[0, 'significant'] = 'YES'
        df_for_export.loc[0, 'effect size: eta^2'] = eta_square
    else:
        df_for_export.loc[0, 'significant'] = 'NO'
        df_for_export.loc[0, 'effect size: eta^2'] = 'N/A'

    if SAVE_PLOTS:
        if parametric:
            df_for_export.to_csv(Path(savepath, savename +'_ANOVA.csv'),index=False)
        else:
            df_for_export.to_csv(Path(savepath, savename + '_kruskal.csv'),
                                 index=False)
            try:
                conover_export.to_csv(Path(savepath, savename + '_conover.csv'),
                                      index=True)
            except UnboundLocalError:
                pass

    print(df_for_export)
    print('\n')
    try:
        print(conover_export)
    except UnboundLocalError:
        pass
    # return(df_for_export)


def flatten_list(nested_list):
    """
    Convenience function to flatten a list
    """
    flat_list = []
    for sublist in nested_list:
        for element in sublist:
            flat_list.append(element)
    return(flat_list)

def calc_SD_pooled(dataset1, dataset2):
    """
    Calculates pooled standard deviation:
    https://www.geeksforgeeks.org/calculate-pooled-standard-deviation-in-python/

    each dataset is a numpy array!
    """
    sd1 = np.std(dataset1)
    n1 = dataset1.shape[0]
    sd2 = np.std(dataset2)
    n2 = dataset2.shape[0]
    pooled_SD = np.sqrt(
        (((n1 - 1) * sd1 * sd1) + ((n2 - 1) * sd2 * sd2)) / (
                    n1 + n2 - 2))

    return (pooled_SD)


def calc_cohens_d(dataset1, dataset2):
    """
    Calculate effect size using Cohen's D
    """
    mean1 = np.nanmean(dataset1)
    mean2 = np.nanmean(dataset2)
    SD_pooled = calc_SD_pooled(dataset1, dataset2)
    cohen = np.abs(np.abs(mean1) - np.abs(mean2)) / SD_pooled

    return (cohen)


def confidence_interval(dataset, level=0.95):
    """
    Calculate confidence interval for one sample.
    """
    return(stats.t.interval(level, dataset.shape[0]-1, loc=np.mean(dataset), scale=stats.sem(dataset)))


def export_pairwise_test(data, savepath, savename, parametric=True,
                         no_of_groups = None):
    """
    This function can be used for a single or multiple pairwise tests.

    In case of multiple tests (identified as nested lists),
    Bonferroni correction is applied and both the original and
    corrected p-value is reported.
    """
    # First, collect the data of variable names below
    test_statistic = []
    p_values = []
    all_labels = []
    mean = []
    median = []
    std = []
    sem = []
    n = []
    ci = []
    effect_size = []
    dof = []

    for current_data in data:

        # export_pairwise_comparison(data):
        labels = current_data.drop_duplicates(subset=["Group"])['Group']  # extract unique group names
        label_list = []
        [label_list.append(i) for i in labels]  # put them into a list
        print(label_list)  # a confirmation that the group names are entered correctly!

        group1_data = \
        current_data[current_data['Group'] == label_list[0]]['median_DTS']
        group2_data = \
        current_data[current_data['Group'] == label_list[1]]['median_DTS']

        mean.append([np.nanmean(group1_data),
                     np.nanmean(group2_data)])

        median.append([np.nanmedian(group1_data),
                       np.nanmedian(group2_data)])

        std.append([np.std(group1_data),
                    np.std(group2_data)])

        n.append([group1_data.shape[0],
                  group2_data.shape[0]])

        sem.append([np.std(group1_data)/group1_data.shape[0],
                    np.std(group2_data)/group2_data.shape[0]])

        ci.append([confidence_interval(group1_data),
                   confidence_interval(group2_data)])

        if parametric:
            test_results = stats.ttest_ind(group1_data, group2_data)
        else:
            test_results = stats.ranksums(group1_data, group2_data)
        test_statistic.append(test_results[0])
        p_values.append(test_results[-1])
        all_labels.append(label_list)

        if parametric:
            effect_size.append(calc_cohens_d(group1_data, group2_data))
        else:
            # According to:
            # r^2 = eta^2 = Z^2/n
            # According to https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.mannwhitneyu.html
            # z is calculated from U1:
            U1, p_ = stats.mannwhitneyu(group1_data, group2_data) # p value not relevant here
            nx, ny = len(group1_data), len(group2_data)
            U2 = nx * ny - U1
            U = min(U1, U2)
            N = nx + ny
            z = (U - nx * ny / 2 + 0.5) / np.sqrt(nx * ny * (N + 1) / 12)
            effect_size.append((z*z)/N)

        dof.append(sum(n[-1])-1)

    ###################################
    # Multiple Test correction using Bonferroni methods
    ###################################

    p_values_np = np.array(p_values)
    if no_of_groups is None:
        corrected_p_values = p_values_np * len(p_values)
    else:
        corrected_p_values = p_values_np * no_of_groups

    #########################################
    # Finally, save the dataframe for export
    #########################################
    df_for_export = pd.DataFrame()

    flat_label_list = flatten_list(all_labels)
    df_for_export["Group"] = flat_label_list

    flat_n = flatten_list(n)
    df_for_export['n'] = flat_n

    flat_mean = flatten_list(mean)
    df_for_export['mean'] = flat_mean

    flat_median = flatten_list(median)
    df_for_export['median'] = flat_median

    flat_std = flatten_list(std)
    df_for_export['STD'] = flat_std

    flat_sem = flatten_list(sem)
    df_for_export['SEM'] = flat_sem

    flat_ci = flatten_list(ci)
    df_for_export['95% CI on the mean'] = flat_ci

    # Next, add values that are only added for every second row as we are always comparing two rows to each other
    for counter, current_index in enumerate(range(0, df_for_export.shape[0], 2)):  # only every second row

        df_for_export.loc[current_index, 'DOF'] = dof[counter]

        if parametric:
            df_for_export.loc[current_index, 'statistical test'] = 'T-test'
        else:
            df_for_export.loc[
                current_index, 'statistical test'] = 'Ranksums test'
        if parametric:
            df_for_export.loc[current_index, 't-statistic'] = test_statistic[counter]
        else:
            df_for_export.loc[current_index, 'rank sum statistic'] = test_statistic[counter]

        df_for_export.loc[current_index, 'p-value'] = p_values_np[
            counter]
        if corrected_p_values.shape[0] > 1:
            df_for_export.loc[current_index, 'Bonferroni corrected p-value'] = \
            corrected_p_values[counter]

        # That will still work as expected as the corrected value will
        # be correct even if there's only one group!
        if corrected_p_values[counter] < 0.05:
            df_for_export.loc[current_index, 'significant'] = 'YES'
            # Only put effect size in case of significant differences
            # of the group
            if parametric:
                df_for_export.loc[current_index, "Effect size: Cohen's D"] = effect_size[counter]
            else:
                df_for_export.loc[current_index, "Effect size: r^2"] = effect_size[counter]
        else:
            df_for_export.loc[current_index, 'significant'] = 'NO'
            # Only put effect size in case of significant differences
            # of the group
            df_for_export.loc[current_index, "Effect size"] = 'N/A'

    if SAVE_PLOTS:
        if parametric:
            df_for_export.to_csv(Path(savepath, savename + '_t_test.csv'))
        else:
            df_for_export.to_csv(Path(savepath, savename + '_ranksums.csv'))

def calc_stats(df_for_stats, labels, parametric=True,
               printout=True):
    """
    Helper function to quickly check for statistical difference. Does
    not do multiple-test correction internally, needs to be done by
    user!

    First, Lilliefors test is used to check for normally distributed data
    Next, Levene's test is used to check for variance between groups.

    The user can use these two tests to either use the parametric t-test
    or, in case the tests above are significant, the non-parametric
    ranksums test to check for significant differences between the two
    groups.
    """
    dict_to_use = {}

    if printout:
        print('comparing ' + labels[0] + ' with ' + labels[1])
    for current_label in labels:
        current_data = \
            df_for_stats[df_for_stats['Group'] == current_label][
                'median_DTS']
        if printout:
            print(current_label)
            # print('shapiro ' + repr(stats.shapiro(current_data)))
            print("lilliefors" + repr(
                sm.stats.lilliefors(current_data)))
            print('\n')
    if printout:
        print('levene: ' + repr(stats.levene(
            df_for_stats[df_for_stats['Group'] == labels[0]][
                'median_DTS'],
            df_for_stats[df_for_stats['Group'] == labels[1]][
                'median_DTS'])))

    if parametric:
        p_value = stats.ttest_ind(
            df_for_stats[df_for_stats['Group'] == labels[0]][
                'median_DTS'],
            df_for_stats[df_for_stats['Group'] == labels[1]][
                'median_DTS'])
        if printout:
            print('t-test test: ' + repr(p_value))
    else:
        p_value = stats.ranksums(
            df_for_stats[df_for_stats['Group'] == labels[0]][
                'median_DTS'],
            df_for_stats[df_for_stats['Group'] == labels[1]][
                'median_DTS'])
        if printout:
            print('ranksums test: ' + repr(p_value))

    dict_to_use[labels[0] + ' vs ' + labels[1]] = p_value[-1]
    return (dict_to_use)

def calc_stats_multiple(df_for_stats, parametric=True):
    """
    Function to test for statistical signifcant differenes between
    more than 2 groups.

    First, Lilliefors test is used to check for normally distributed data
    Next, Levene's test is used to check for variance between groups.

    The user can use these two tests to either use the parametric ANOVA
    or, in case the tests above are significant, the non-parametric
    Kruskal-Wallist test to check for significant differences between
    the all groups.

    To establish which of the tested groups are significantly different
    either the pairwise Tukey HSD test is used following the ANOVA or
    conover test following the Kruskal-Wallis test.
    """

    data_for_anova = []
    for name_group in df_for_stats.groupby('Group'):
        current_data = name_group[1]['median_DTS']
        print(
            "lilliefors " + repr(sm.stats.lilliefors(current_data)))
        data_for_anova.append(current_data)

    print('levene ' + repr(stats.levene(*data_for_anova)) + '\n\n')

    if parametric:
        print(stats.f_oneway(*data_for_anova))
        print('\n')
        # print pairwise_tukeyhsd(Data, Group)
        print(pairwise_tukeyhsd(df_for_stats['median_DTS'],
                                df_for_stats['Group']))
    else:
        print(stats.kruskal(*data_for_anova))
        print('\n')
        print(scikit_posthocs.posthoc_conover(df_for_stats,
                                              val_col='median_DTS',
                                              group_col='Group',
                                              p_adjust='holm'))

