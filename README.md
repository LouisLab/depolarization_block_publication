# Depolarization Block Publication:

This repository contains the scripts to create all figures shown in the manuscript: https://www.science.org/doi/full/10.1126/sciadv.ade7209

#### Depolarization block of olfactory sensory neurons adds a new dimension to odor encoding

---

This repository contains 

1) The STL file to create the 3D printed 'fruit cup' in the folder '3D file'.

2) Jupyter notebook and functions used to create all Figures in the 
   manuscript 'Depolarization block of olfactory sensory neurons 
   adds a new dimension to odor encoding' in the folder 'code_to_plot_figures'.

3) Code of the Or42b OSN simulation in the folder 'code_for_simulations'.

4) Code to calculate PSTH from electrophysiology data in folder 'code_other'

---

## Description of the Data and file structure

Please see README.md in each folder for more information on how to run the scripts. 

This repository is accompanied by datafiles found on datadryad: https://doi.org/10.25349/D92K69