# Calculation of PSTH

After spike sorting using the HDBSCAN_spike_sorter (https://gitlab.com/LouisLab/hdbscan_spike_sorter), 
this script (calculate_PSTH_from_data.py) is used to extract the PSTH. 

To run this script, follow the instructions in 'code_to_plot_figures/README.md' and run this script using python.