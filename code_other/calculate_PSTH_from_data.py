"""
This script calculate the PSTH for each experiment.

It is essentially a translation of the Matlab script used in
Schulze et al., 2015.

Note on the data format:
The odor pipette has several channels. I always used a maximum of 4.
These are indicates as 'pCh'. pCh4 was ALWAYS the saline while pCh1,
pCh2 and pCh3 contained odor as indicated in the experimental folder
name
"""

import json
import pickle
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pyabf
from scipy.signal import lfilter
from natsort import natsorted
from scipy import interpolate

fSample = 10000 # All recordings were done at 10kHz
tStep = 1/fSample

# Root Path is the folder to analyze
root_path = 'G:\\My Drive\\PhD\\Odor quality discrimination\\ephys\\analysis\\Or42aSF MA'

export_PSTH_individual_experiment = False # might only be necessary
# for step stimulus - downside is large file size

original_export = False # Use this for mixed length of stimulus, currently
# only Or42b EtB 10uM (set to False)

# Save for Philip
save_path = Path(
            'G:\\Shared drives\\LouisLab COLLABORATIVE Odor '
            'Quality Discrimination Project\\ephys\\PSTH')
save_for_sim=False

# List of folders that have more than one folder (e.g. exponential, gap 1s)
folders_1 = [#'exponential',
             #'gap 1s',
             #'gap 500ms',
             #'pulse 1s',
             #'ramp 10s',
             #'sigmoid',
             'step stim',
             #'trajectories',
             #'Hysteresis2'
             #'step down'
             #'step_with_flicker'
             #'no_stim'
             ]

# List of folders which are directly analyzed
folders_2 = [#'step stim\\EtA 100uM\\EtA 100uM 200mbar',
             #'step stim\\EtA 1000uM\\EtA 1000uM 200mbar'
             ]

#% Params for PSTH
resolution = 33 # milliseconds
binSize = resolution/1000 # bring back to seconds

filtH = 0.005*fSample
# PROBLEM: I can't use an even sized filter! The number must be odd.
# If even size there are two indeces that are 'the peak' instead of a
# single peak.
if filtH%2 == 0:
    filtH -= 1
HSIZE = [1, filtH]
SIGMA = 1

def matlab_style_gauss2D(shape=(3,3),sigma=0.5):
    """
    2D gaussian mask - gives the same result as MATLAB's
    fspecial('gaussian',[shape],[sigma]) used in the original
    Matlab script
    from: https://stackoverflow.com/questions/17190649/how-to-obtain-a-gaussian-filter-in-python
    """
    m,n = [(ss-1.)/2. for ss in shape]
    y,x = np.ogrid[-m:m+1,-n:n+1]
    h = np.exp( -(x*x + y*y) / (2.*sigma*sigma) )
    h[ h < np.finfo(h.dtype).eps*h.max() ] = 0
    sumh = h.sum()
    if sumh != 0:
        h /= sumh
    return h
H = matlab_style_gauss2D(HSIZE, SIGMA)

#% Use an exponential filter to smoothen the PSTH later - so as to
# prevent the appearance of "pre-stimulus" responses
alpha = 0.5 #% higher alpha = less smoothing
########################################################################
def get_median_measurement(lines):
    '''
    It's a bit of a  pain that the LED measurement csv file isn't
    properly formatted. The below should work, however:
    It's looking for the line that contains the word 'Samples' as
    the following lines contain the measurements.
    '''
    current_measurements = []
    get_values = False
    for l in lines:
        if get_values:
            current_measurements.append(np.float(l.split(';')[-1]))
        else:
            if 'Samples' in l.split(';')[0]:
                get_values = True

    return (np.median(np.asarray(current_measurements)))
def volt_to_watt():

    # This collects the uW/mm2 values - no need to change anything
    root_path = Path('G:\\My Drive\\PhD\\Papers\\Vanishing spikes\\all_data\\other_data')
    path = Path(str(root_path), '2xND6 - S170C')
    # Sorting the path is a bit of a pain...
    # this code snippet sorts the paths into an ascending list
    voltages = []
    for i in path.iterdir():
        if 'csv' in i.name:
            voltages.append(float(i.name.split('V')[0]))
    sorted_voltages = natsorted(voltages)
    sorted_paths = []
    for i in sorted_voltages:
        sorted_paths.append(Path(path, str(i) + 'V.csv'))
    all_measurements_W = np.zeros((len(sorted_paths),2))

    for counter, current_file in enumerate(sorted_paths):
        all_measurements_W[counter,0] = np.float(current_file.name.split('V.csv')[0])
        # Loop the data lines
        with open(current_file, 'r') as file:
            # read the file
            lines = file.readlines()
            # get the median measurement value
            all_measurements_W[counter,1] = \
                get_median_measurement(lines)
    all_measurements_uW = all_measurements_W.copy()
    all_measurements_uW[:,1] = all_measurements_W[:,1] * 1e6 # convert to uW

    # The value that is needed for comparison is the light intensity per area, not the absolute light intensity given above.
    # The objective used for the experiments was a Olympus LUMPLFLN 40XW. It has a field number of 26.5 and a magnification
    # of 40. The diamater of the field size is therefore: 26.5 / 40 = 0.6625mm.
    # Circle with diameter of 0.6625mm is: (0.6625mm/2)^2 * pi = 0.345mm^2.
    # The area of the circle is calculated for the focal plane, which might differ for the neuron that was recorded from and
    # the neuron that was activated.

    uW_per_mm = all_measurements_uW.copy()
    uW_per_mm[:, 1] /= 0.345  # light intensity is now in uW/mm2

    # As we don't always have voltages defined at exactly the values we measured (for example during a ramp)
    # I am interpolating the values in between with the following function.
    volt_to_watt_func = interpolate.interp1d(uW_per_mm[:,0],
                                              uW_per_mm[:,1])
    watt_to_volt_func = interpolate.interp1d(uW_per_mm[:,1],
                                              uW_per_mm[:,0])
    # needed for coding
    lowest_measured_voltage = uW_per_mm[:,0].min()
    highest_measured_voltage = uW_per_mm[:,0].max()

    return(volt_to_watt_func,
           watt_to_volt_func,
           lowest_measured_voltage,
           highest_measured_voltage)

##########################################################################

# Collect the spike data
def calc_PSTH(i_stimulus, save_for_sim=True,save_path=None,
              original_export=True):
    print(i_stimulus)
    spikes_all = []
    LED_all = []
    odor_all = []
    folder_name = []
    # sort the folders by channel used: start with pCh1, the pCh2 and
    # finally pCh3. This will help with making sure we always get the
    # correct odor channel out
    pCh1_folders = []
    pCh2_folders = []
    pCh3_folders = []
    for i_folder in i_stimulus.iterdir():
        if i_folder.is_dir():
            if 'pCh1' in i_folder.name:
                pCh1_folders.append(i_folder)
            elif 'pCh2' in i_folder.name:
                pCh2_folders.append(i_folder)
            elif 'pCh3' in i_folder.name:
                pCh3_folders.append(i_folder)
            else:
                print('something wrong with folder ' + i_folder)

    all_folders = pCh1_folders + pCh2_folders + pCh3_folders

    for i_folder in all_folders:
        print(i_folder)
        for i_file in Path(i_folder).iterdir():
            if i_file.name.endswith('.abf'):
                abf = pyabf.ABF(str(i_file))
            if '_final_spikes.pkl' in str(i_file): # This is the output from the HDBSAN_spike_sorter
                with open(str(i_file), 'rb') as f:
                    spikes = pickle.load(f)
                # also grab foldername here
                folder_name.append(i_folder.name)

        # print(abf.headerText) # Use this to find info in ABF file
        # create empty array for numpy array for better handling
        spikes_np = np.zeros((abf.data.shape[1]))
        # indicate each spike with a one (the rest stays zero)
        spikes_np[np.array(spikes['target_spikes_1'])] = 1
        LED_all.append(spikes['LED']) # the light stimulus
        odor_all.append(spikes['Odor stim']) # the odor stimulus

        spikes_all.append(spikes_np)

    # In case of different length between experiments.
    longest_exp = 0
    for i in range(len(spikes_all)):
        if spikes_all[i].shape[0] > longest_exp:
            longest_exp = spikes_all[i].shape[0]

    used_length = []
    LED_files = []
    for i in range(len(LED_all)):
        if LED_all[i].shape[0] in used_length:
            pass
        else:
            used_length.append(LED_all[i].shape[0])
            LED_files.append(LED_all[i])
    if len(used_length) > 1:
        # This will only work if we have only two different sizes,
        # which is the maximum used in this project.
        if used_length[0] > used_length[1]:
            long_length = used_length[0]
            short_length = used_length[1]
            long_start_index = np.where(LED_files[0] > 0.1)[0][0]
            short_start_index = np.where(LED_files[1] > 0.1)[0][0]
        else:
            long_length = used_length[1]
            short_length = used_length[0]
            long_start_index = np.where(LED_files[1] > 0.1)[0][0]
            short_start_index = np.where(LED_files[0] > 0.1)[0][0]

        correct_index_by = long_start_index - short_start_index

        LED_all_temp = np.zeros((len(LED_all), longest_exp))
        LED_all_temp.fill(np.nan)
        for i in range(LED_all_temp.shape[0]):
            if LED_all[i].shape[0] == long_length:
                LED_all_temp[i, 0:LED_all[i].shape[0]] = LED_all[i]
            elif LED_all[i].shape[0] == short_length:
                LED_all_temp[i,correct_index_by:LED_all[i].shape[0] + correct_index_by] = \
                    LED_all[i]
        LED_all = LED_all_temp

        # To check if the alignment is correct
        # fig,ax=plt.subplots()
        # ax.plot(LED_all.T)

        spikes_all_temp = np.zeros((len(spikes_all), longest_exp))
        spikes_all_temp.fill(np.nan)
        for i in range(spikes_all_temp.shape[0]):
            if spikes_all[i].shape[0] == long_length:
                spikes_all_temp[i, 0:spikes_all[i].shape[0]] = \
                spikes_all[i]

            elif spikes_all[i].shape[0] == short_length:
                spikes_all_temp[i, correct_index_by:spikes_all[i].shape[
                                                        0] + correct_index_by] = \
                    spikes_all[i]
        spikes_all = spikes_all_temp

        odor_all_temp = np.zeros((len(odor_all), longest_exp, 4))
        odor_all_temp.fill(np.nan)
        for i in range(odor_all_temp.shape[0]):
            if odor_all[i].shape[0] == long_length:
                odor_all_temp[i, 0:odor_all[i].shape[0], :] = odor_all[
                    i]
            else:
                odor_all_temp[i, correct_index_by:odor_all[i].shape[
                                                      0] + correct_index_by] = \
                odor_all[i]
        odor_all = odor_all_temp
    else:
        LED_all = np.array(LED_all)
        spikes_all = np.array(spikes_all)
        odor_all = np.array(odor_all)

    ####################################################################
    # It is necessary to adjust the spike number between experiments
    # based on when the odor stimulus started as the time before the
    # odor stimulus is sometimes different
    odor_channel_index = 0
    # Find onset of odor stimulus
    if not original_export:
        temp_indeces = odor_all[:, :, odor_channel_index] > 0.75

        # find the intial index for each trial
        start_index = []
        for i_current_trial in range(temp_indeces.shape[0]):
            start_index.append(
                np.where(temp_indeces[i_current_trial, :] > 0)[0][0])
        start_index = np.array(start_index)
        # The start index is in the abf file format time

        for i_current_trial in range(1, start_index.shape[0]):
            diff = start_index[0] - start_index[i_current_trial]

            spikes_all[i_current_trial,:] = np.roll(spikes_all[i_current_trial,:], diff)

    ####################################################################

    length_bins = spikes_all.shape[1] / (binSize * fSample)
    increment = int(np.floor(spikes_all.shape[1] / length_bins))
    # an easier way to calculate increment would be:
    # binsize = 0.033 # 33ms in s
    # fs = 10000 # aquisition frequency
    # increment = binsize * fs

    ########################################################################
    filtPSTHPerTrial = np.zeros((spikes_all.shape[0],
                                 int(np.ceil(length_bins))))
    unfilteredPSTHPerTrial = np.zeros((spikes_all.shape[0],
                                       int(np.ceil(length_bins))))

    for i_exp in range(spikes_all.shape[0]):
        # filtering of spikes with the above defind gaussian filter 'H'
        filteredSpikes = spikes_all[i_exp, :]
        filteredSpikes = np.convolve(filteredSpikes, H[0, :], 'same')

        # When reshaping I need to take into account that I can't just
        # take any size: E.g. I can't reshape 50000 into 330 bins.

        summed = []
        for i_bin in np.arange(0, filteredSpikes.shape[0], 330):
            # Note: the third parameter in np.arange just indicates the
            # difference between the values. It returns an array like this:
            # 0, 330, 660 etc.
            try:
                summed.append(
                    np.nansum(filteredSpikes[i_bin:i_bin + increment]))
            except IndexError:
                pass
        summed = np.array(summed)

        corrected = summed * (1 / binSize)

        psthFiltOut = corrected

        unfilteredPSTHPerTrial[i_exp, :] = psthFiltOut.copy()

        # https://www.mathworks.com/help/matlab/ref/filter.html
        # According to the website the matlab script implements the
        # following function:
        # H(z) = b(1)/(a(1)+a(2)*z^-1).
        # in the matlab script
        # b = alpha = 0.5
        # a = [1, alpha-1] = [1, -0.5]
        # According to https://stackoverflow.com/questions/8922657/matlab-filter-with-scipy-lfilter
        # the scipy function lfilter should be analog to the matlab
        # filter function

        # this definition of b is necessary as I need a 1D array
        b = np.zeros(1)
        b[0] = alpha
        filtPSTHPerTrial[i_exp, :] = lfilter(b=b,
                                             a=np.array((1, alpha - 1)),
                                             x=corrected)
    #######################################################################
    fig = plt.figure(figsize=(20, 10))
    ax_top = fig.add_subplot(311)
    ax_middle = fig.add_subplot(312, sharex=ax_top)
    ax_bottom = fig.add_subplot(313, sharex=ax_top)
    #######################################################################
    # The spike filtPSTHPerTrial has a much lower sampling frequency
    # as defined as 33 ms above (variable 'resolution).
    time = np.linspace(0, spikes_all.shape[1] / fSample,
                       filtPSTHPerTrial.shape[1])
    ################################################

    spike_freq = np.nanmean(filtPSTHPerTrial, axis=0)
    spike_freq_std = np.nanstd(filtPSTHPerTrial, axis=0)
    spike_freq_sem = spike_freq_std/filtPSTHPerTrial.shape[0]
    ax_top.plot(time, spike_freq, c='k', zorder=0)
    ax_top.fill_between(time, spike_freq - spike_freq_std,
                        spike_freq + spike_freq_std,
                        color='b', alpha=0.5, zorder=0)
    ax_top.set_ylabel('Firing frequency [Hz]')
    #######################################################################
    for i_trial in range(spikes_all.shape[0]):
        ax_middle.eventplot(
                np.where(spikes_all[i_trial, :] == 1)[0] / fSample,
                linewidth=0.3, lineoffsets=i_trial, color='k')
    #########################################################################
    # LED, convert voltage to watt
    LED_voltage = np.nanmean(LED_all, axis=0)
    volt_to_watt_func, watt_to_volt_func, lowest_measured_voltage, highest_measured_voltage = volt_to_watt()
    # Indeces where we need to insert the uW/mm2
    indeces_to_change_LED = np.where(
        LED_voltage >= lowest_measured_voltage)[0]
    # empty array
    uW_array = np.zeros((LED_voltage.shape[0]))
    # fill empty array with the uW/mm2 values only where voltage is above the threshold
    for i in indeces_to_change_LED:
        try:
            uW_array[i] = volt_to_watt_func(LED_voltage[i])
        except:
            print('out of interpolation range: ' + repr(LED_voltage[i])
                  + '\nchanged to: ' + repr(
                highest_measured_voltage))
            uW_array[i] = volt_to_watt_func(
                highest_measured_voltage)

    time = np.linspace(0, LED_all.shape[1] / fSample, LED_all.shape[1])
    ax_bottom.plot(time, uW_array, c='r', label='LED')
    ##
    odor_colors = ['b', 'g', 'm', 'k']
    if not (odor_all == 0).all():
        ax_bottom_right = ax_bottom.twinx()
        for i_odor in range(odor_all.shape[2]):
            ax_bottom_right.plot(time, np.nanmedian(odor_all, axis=0)[:,
                                       i_odor],
                                 c=odor_colors[i_odor],
                                 label='Odor Ch:' + repr(
                                     int(i_odor + 1)))

        ax_bottom_right.set_ylabel('Pressure [mbar]')

    ax_bottom.set_ylabel('LED [uW/mm2]')
    ax_bottom.set_xlabel('Time [s]')

    #print(spike_freq[~np.isnan(spike_freq)].max())

    fig.savefig(Path(i_stimulus, 'PSTH.png'))
    if np.nanmax(odor_all[0, :, 0]) > 10 and \
        np.nanmax(odor_all[0, :, 1]) > 10 and \
        np.nanmax(odor_all[0, :, 2]) > 10:
        odor_channel = [0, 1, 2]
    elif np.nanmax(odor_all[0, :, 0]) > 10 and np.nanmax(odor_all[0, :, 1]) > 10:
        odor_channel = [0, 1]
    elif np.nanmax(odor_all[0, :, 0]) > 10:
        odor_channel = 0
    elif np.nanmax(odor_all[0, :, 1]) > 10:
        odor_channel = 1
    elif np.nanmax(odor_all[0,:, 2]) > 10:
        odor_channel = 2
    else:
        odor_channel = None

    # Save PSTH in a single csv file for each folder
    ###################################################################
    #

    # Different folder structure...
    if i_stimulus.parts[-4] == 'analysis':
        genotype = i_stimulus.parts[-3]
        stimulus = i_stimulus.parts[-1]
    else:
        genotype = i_stimulus.parts[-4]
        stimulus = i_stimulus.parts[-1]
    print(genotype)
    print(stimulus)

    #
    if 'ChrimsonR' in i_stimulus.parts[-1] or 'noOdor' in i_stimulus.parts[-1]:
        indeces_to_take = np.ones((LED_all.shape[1]), dtype=np.bool)
    else:

        # Here I define indeces of when OB1 was on!
        indeces_to_take = odor_all[0, :, odor_channel_index] > 0.75 # sometimes noisy around 1.


    #####
    # The spike frequency is now on a completely different timescale
    # compared to the  and odor stimulus: E.g. We have 1516 bins
    # vs 500'000 LED stimulus.
    # We'll need to put the spike frequency from 1516 bins on
    # 500'000 and hence expand the dataset
    extended_spike_freq = np.zeros((LED_all.shape[1]))
    extended_spike_freq.fill(np.nan)
    for counter, i_ext in enumerate(
            np.arange(0, LED_all.shape[1], increment)):
        # print(spike_freq[counter])
        extended_spike_freq[i_ext:i_ext + increment] = spike_freq[
            counter]

    # Do same for the standard deviation
    extended_spike_freq_std = np.zeros((LED_all.shape[1]))
    extended_spike_freq_std.fill(np.nan)
    for counter, i_ext in enumerate(
            np.arange(0, LED_all.shape[1], increment)):
        # print(spike_freq[counter])
        extended_spike_freq_std[i_ext:i_ext + increment] = \
            spike_freq_std[counter]

    # and for SEM
    extended_spike_freq_sem = np.zeros((LED_all.shape[1]))
    extended_spike_freq_sem.fill(np.nan)
    for counter, i_ext in enumerate(np.arange(0, LED_all.shape[1], increment)):
        extended_spike_freq_sem[i_ext:i_ext + increment] = spike_freq_sem[counter]

    export = pd.DataFrame()
    export['Time [s]'] = np.linspace(0, indeces_to_take.sum() / fSample,
                                     indeces_to_take.sum())

    export['PSTH [Hz]'] = extended_spike_freq[indeces_to_take]
    export['PSTH STD'] = extended_spike_freq_std[indeces_to_take]
    export['PSTH SEM'] = extended_spike_freq_sem[indeces_to_take]

    for counter_file, i_name in enumerate(folder_name):
        # save the individual spikes so that
        # I can do a raster plot using this csv file!
        current_spikes = spikes_all[counter_file][indeces_to_take]
        export['spikes_' + i_name] = current_spikes[0:export.shape[0]]

        # if necessary, also save PSTH
        if export_PSTH_individual_experiment:
            extended_individiual_PSTH = np.zeros((LED_all.shape[1]))
            extended_individiual_PSTH.fill(np.nan)
            for counter, i_ext in enumerate(
                    np.arange(0, LED_all.shape[1], increment)):
                # print(spike_freq[counter])
                extended_individiual_PSTH[i_ext:i_ext + increment] = \
                    filtPSTHPerTrial[counter_file, counter]
            export['PSTH_' + i_name] = extended_individiual_PSTH[indeces_to_take]

    # Manually enter which odor channels are used.
    if odor_channel == [0, 1, 2]:
        export['pressure Ch1 [mbar]'] = odor_all[
            0, indeces_to_take, odor_channel[0]]
        export['pressure Ch2 [mbar]'] = odor_all[
            0, indeces_to_take, odor_channel[1]]
        export['pressure Ch3 [mbar]'] = odor_all[
            0, indeces_to_take, odor_channel[2]]
    elif odor_channel == [0, 1]:
        export['pressure Ch1 [mbar]'] = odor_all[
            0, indeces_to_take, odor_channel[0]]
        export['pressure Ch2 [mbar]'] = odor_all[
            0, indeces_to_take, odor_channel[1]]
    elif odor_channel is not None:
        export['pressure [mbar]'] = odor_all[
            0, indeces_to_take, odor_channel]
    else:
        export['pressure [mbar]'] = np.zeros((LED_all.shape[1]))
    export['light [uW/mm]'] = uW_array[indeces_to_take]

    export.to_csv(Path(i_stimulus,  # datetime.datetime.now().strftime(
                       # '%Y-%m-%d_%H-%M-%S') + '_'+ \
                       genotype + '_' + stimulus + '.csv'))
    if save_for_sim:
        # Directly put it into a shared folder for modeling
        save_path = Path(save_path, genotype)
        Path(save_path).mkdir(parents=True, exist_ok=True)
        export.to_csv(Path(save_path,
                           genotype + '_' + stimulus + '.csv'))

        # This helps to make sure that we're actually extracting the
        # correct odor stimulus
        fig = plt.figure()
        ax_top = fig.add_subplot(211)
        ax_top.plot(export['PSTH [Hz]'])
        ax_bottom = fig.add_subplot(212)
        if 'ChrimsonR' in i_stimulus.parts[-1]:
            ax_bottom.plot(export['light [uW/mm]'])
        else:
            ax_bottom.plot(export['pressure [mbar]'])

        try:
            with open(Path(save_path,
                           genotype + '_n_overview.json')) as data_file:
                n_file = json.load(data_file)
        except FileNotFoundError:
            n_file = {}

        n_file[i_stimulus.name] = filtPSTHPerTrial.shape[0]

        with open(Path(save_path, genotype + '_n_overview.json'),
                  'w') as outfile:
            json.dump(n_file, outfile, sort_keys=True, indent=4)

        #plt.show(block=True)

########################################################################
# This will only work for folders_1 !!
for i_condition in range(len(folders_1)):
    current_path = Path(root_path, folders_1[i_condition])
    print(current_path)
    # e.g. G:/My Drive/PhD/Odor quality discrimination/ephys/analysis/Or42aSF/exponential
    for i_stimulus in current_path.iterdir():
        if i_stimulus.is_dir():
            # e.g. 'G:/My Drive/PhD/Odor quality discrimination/ephys/analysis/Or42aSF/
            # exponential/EtA 100uM exponential'
            calc_PSTH(i_stimulus, save_for_sim=save_for_sim,
                      save_path=save_path, original_export=original_export)

########################################################################

# This is for folders_2:
for i_condition in range(len(folders_2)):
    current_path = Path(root_path, folders_2[i_condition])

    calc_PSTH(current_path, save_for_sim=save_for_sim,
              save_path=save_path, original_export=original_export)